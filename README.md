# Peppered Software Git Workflow Guide

This guide documents the git workflow used by Peppered Software for all software projects.

## Introduction

Here are some links to learn more about the git version control system in general.
- [Pro Git](http://git-scm.com/book)
- [Git wikipedia](http://en.wikipedia.org/wiki/Git_(software))

## Basic Git Branching Structure

All projects will have, at minimum, two branches.

* **Master** - This is the production branch.  Commits to master should happen only when in concert with a production release.
* **Develop** - This is the main branch for all development.  This is the branch that will be tested and continuously integrated with build system.

Commits to the “master” branch will only ever occur from the “develop” branch.

## Git Workflow

When completing a task, fixing a bug, or doing any other unit of work, branch from the latest commit to the “develop” branch.  Each branch should correspond to exactly one “issue”.  If you’re working on an item captured in task-tracking software, name the branch with the issue number followed by a few words about the branch.  All tasks should be named with the prefix `feature/`

**For Example:**
```
git branch feature/dw_123_localization
```

**Not:**
```
git branch fixes
```

If a task number is not available, make your branch name as clear as possible.

**For Example:**
```
git branch feature/homescreen_animation_timing
```

**Not:**
```
git branch animation
```

## Pushing your work up to origin

Whenever you get up from your work you should push your branch up to origin, completed or not.

```
git push origin my_branch_name
```

## Merging your branch into Develop

When you feel a branch is ready to be merged into develop, create a “pull request” on Bitbucket or Github.  Documentation on pull requests from Bitbucket can be found [here](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests).

Assign your team leader to review the pull request.  *Never* merge your branch directly into develop without going through a pull request.

## Preparing your branch for a Pull Request

All pull requests should squash commits in your feature branch down to a single commit.  This is to keep the git log as clean as possible.

To squash your commits, use Git’s interactive rebase feature.

```
git rebase -i HEAD~[number of commits you made on your branch]
```

So if I made 4 commits on my feature branch:
```
git rebase -i HEAD~4
```

After rebasing, you should have a single commit with a very clear commit message and description.  Pull requests can be rejected for bad commit messages or descriptions.

## Keeping your branch up to date

There are times when the Develop branch will get ahead of your branch.  Before setting down to work or pushing your work up to origin, make sure you are up to date with develop.

We use rebasing instead of merging to avoid commit noise.

**To update your branch with the latest from develop:**
```
git fetch
git rebase origin/develop
```

Fix any conflicts that may arise and then resume work on your branch.